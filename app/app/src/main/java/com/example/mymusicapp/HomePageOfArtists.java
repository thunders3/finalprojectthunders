package com.example.mymusicapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.LocusId;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class HomePageOfArtists extends AppCompatActivity {
    ImageView imageView;
    Button button;
    ListView listView;
    EditText editText;
    Button searchButton;
    ArrayList<String> myArrayList;
    ArrayAdapter<String> myArrayAdapterList;
    ArrayAdapter<String> myArrayAdapterList2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_artist_by_name);
        //imageView = (ImageView) findViewById(R.id.imageView1);
        //button = (Button) findViewById(R.id.readMoreButton);
        searchButton = (Button) findViewById(R.id.searchButton);
        editText = (EditText) findViewById(R.id.searchArtist);
        listView = (ListView) findViewById(R.id.listView);
        myArrayList = new ArrayList<>();
        myArrayAdapterList= new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,myArrayList);
        myArrayAdapterList2 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_2,myArrayList);


       searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                get_json();
            }
        });

    }

    public void get_json(){
        String json;
        try {
            InputStream inputStream = getAssets().open("json1.json");
            int size= inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, StandardCharsets.UTF_8);
            JSONArray jsonArray = new JSONArray(json);
            for (int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                if(jsonObject.getString("Name").equals(editText.getText().toString()))
                {

                    myArrayList.add(jsonObject.getString("Song" ));
                    myArrayList.add(jsonObject.getString("Album"));
                    myArrayList.add(jsonObject.getString("Year"));
                    myArrayList.add(jsonObject.getString("Nationality"));
                    myArrayList.add(jsonObject.getString("Short Biography"));
                    listView.setAdapter(myArrayAdapterList);
                    myArrayAdapterList.notifyDataSetChanged();

                }


            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (JSONException e){}
    }

}