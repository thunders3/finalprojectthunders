package com.example.mymusicapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;

public class Login extends AppCompatActivity {
    private Button skipButton;
    private MaterialButton loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView username = findViewById(R.id.username);
        TextView password = findViewById(R.id.password);

        loginButton = (MaterialButton) findViewById(R.id.loginButton);
        skipButton = (Button) findViewById(R.id.skipButton);

        //admin and admin

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (username.getText().toString().equals("admin") && password.getText().toString().equals("admin")) {
                    //correct
                    Toast.makeText(Login.this, "LOGIN", Toast.LENGTH_SHORT).show();
                    startActivity((new Intent(getApplicationContext(), FetchArtistData.class)));

                } else
                    //incorrect
                    Toast.makeText(Login.this, "ERROR", Toast.LENGTH_SHORT).show();
            }
        });
        skipButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Toast.makeText(Login.this,"You skipped.",Toast.LENGTH_SHORT).show();
                openListOfSongsFromPhone();
            }
        });
    }
    public void openListOfSongsFromPhone() {
        Intent intent = new Intent(this, HomePageOfArtists.class );
        startActivity(intent);

    }
}