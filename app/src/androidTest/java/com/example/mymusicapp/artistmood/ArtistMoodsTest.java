package com.example.mymusicapp.artistmood;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.*;

import android.app.Activity;
import android.app.Instrumentation;
import android.view.View;

import androidx.test.rule.ActivityTestRule;

import com.example.mymusicapp.R;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class ArtistMoodsTest {

    @Rule
    public ActivityTestRule<ArtistMoods> mActivityTestRule = new ActivityTestRule<ArtistMoods>(ArtistMoods.class);
    private ArtistMoods mActivity = null;
    Instrumentation.ActivityMonitor monitor = getInstrumentation().addMonitor(HappyActivity.class.getName(),null,false);

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
    }

    @Test
    public void testLaunchOfHappyActivityOnButtonClick(){
        assertNotNull(mActivity.findViewById(R.id.happyBtn));
        onView(withId(R.id.happyBtn)).perform(click());
        Activity happyActivity = getInstrumentation().waitForMonitorWithTimeout(monitor,5000);
        assertNotNull(happyActivity);
    }

    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }
}