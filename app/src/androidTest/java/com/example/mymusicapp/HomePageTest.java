package com.example.mymusicapp;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
@LargeTest

public class HomePageTest {

    @Rule
    public ActivityScenarioRule<HomePage> activityRule
            = new ActivityScenarioRule<>(HomePage.class);

    @Test
    public void useAppContext() throws InterruptedException {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.example.mymusicapp", appContext.getPackageName());

        // Type text and then press the button.
        onView(withId(R.id.btn1))
                .perform(click());
        onView(withId(R.id.btn2))
                .perform(click());
        onView(withId(R.id.btn3))
                .perform(click());
        onView(withId(R.id.btnFvrList))
                .perform(click());
        pressBack();
        Thread.sleep(6000);
        onView(withId(R.id.btn1))
                .perform(click());
        onView(withId(R.id.btn2))
                .perform(click());
        onView(withId(R.id.btn3))
                .perform(click());
        onView(withId(R.id.btnFvrList))
                .perform(click());
        pressBack();

        //Kalhspera Nikos Koukos onomazomai

    }


}