package com.example.mymusicapp.fetchartistinfo;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

import android.content.Context;

import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import com.example.mymusicapp.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class SearchAlbumsAndMusicVideosByArtistNameTest {
    @Rule
    public ActivityScenarioRule<SearchAlbumsAndMusicVideosByArtistName> activityRule
            = new ActivityScenarioRule<>(SearchAlbumsAndMusicVideosByArtistName.class);

    @Test
    public void TestSearchAlbumsByArtistName() throws InterruptedException {
        //  Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.example.mymusicapp", appContext.getPackageName());

        // Type text and then press the button.
        onView(ViewMatchers.withId(R.id.et_dataInPut))
                .perform(typeText("Michael Jackson"),closeSoftKeyboard());
        onView(withId(R.id.albumsByID))
                .perform(click());

        Thread.sleep(3000);


    }
    @Test
    public void TestSearchMusicVideosByArtistName() throws InterruptedException {
        //  Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.example.mymusicapp", appContext.getPackageName());

        // Type text and then press the button.
        onView(ViewMatchers.withId(R.id.et_dataInPut))
                .perform(typeText("Rihanna"),closeSoftKeyboard());
        onView(withId(R.id.musicVideos))
                .perform(click());

    }
    @Test
    public void TestSearchAndClickOnMusicVideosByArtistName() throws InterruptedException {
        //  Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.example.mymusicapp", appContext.getPackageName());
        List<ArtistMusicVideosModel> artistMusicVideosModels;

        // Type text and then press the button.
        onView(ViewMatchers.withId(R.id.et_dataInPut))
                .perform(typeText("Eminem"),closeSoftKeyboard());
        onView(withId(R.id.musicVideos))
                .perform(click());
        onView(withId(R.id.lv_ArtistData))
                .perform(click());

    }
    @Test
    public void TestSearchAlbumsByArtistNameBothClicks() throws InterruptedException {
        //  Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.example.mymusicapp", appContext.getPackageName());

        // Type text and then press the button.
        onView(ViewMatchers.withId(R.id.et_dataInPut))
                .perform(typeText("The Weeknd"),closeSoftKeyboard());
        onView(withId(R.id.albumsByID))
                .perform(click());
        Thread.sleep(2000);
        onView(withId(R.id.musicVideos))
                .perform(click());
        Thread.sleep(2000);

    }

}