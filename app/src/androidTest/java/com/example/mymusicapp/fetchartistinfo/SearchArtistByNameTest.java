package com.example.mymusicapp.fetchartistinfo;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

import android.content.Context;

import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import com.example.mymusicapp.R;
import com.example.mymusicapp.fetchartistinfo.SearchArtistByName;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class SearchArtistByNameTest {

    @Rule
    public ActivityScenarioRule<SearchArtistByName> activityRule
            = new ActivityScenarioRule<>(SearchArtistByName.class);

    @Test
    public void TestSearchArtistByNameClass() throws InterruptedException {
       //  Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.example.mymusicapp", appContext.getPackageName());

        // Type text and then press the button.
       onView(ViewMatchers.withId(R.id.et_dataInput))
               .perform(typeText("Lady Gaga"),closeSoftKeyboard());
        onView(withId(R.id.btn_getInfoByArtistName))
                .perform(click());

        Thread.sleep(5000);


    }

}