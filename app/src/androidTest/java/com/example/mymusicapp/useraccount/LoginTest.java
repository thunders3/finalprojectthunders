package com.example.mymusicapp.useraccount;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;


import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


//import androidx.test.ext.junit.runners.AndroidJUnit4;
//import androidx.test.platform.app.InstrumentationRegistry;

import com.example.mymusicapp.R;
import com.example.mymusicapp.useraccount.Login;

//import com.example.mymusicapp.AssetManagerReader;
//import com.example.mymusicapp.R;
//import com.example.mymusicapp.audiodbclient.ArtistInfoClient;
//import com.example.mymusicapp.audiodbclient.ArtistInfoRequest;
//import com.example.mymusicapp.dto.ArtistDto;

@RunWith(AndroidJUnit4.class)
@LargeTest

public class LoginTest {

//    @Rule
//    public ActivityScenarioRule<SearchArtistByName> activityRule
//            = new ActivityScenarioRule<>(SearchArtistByName.class);
@Rule
public ActivityScenarioRule<Login> activityRule
        = new ActivityScenarioRule<>(Login.class);


    @Test
    public void useAppContext() throws InterruptedException {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.example.mymusicapp", appContext.getPackageName());

        // Type text and then press the button.
        onView(ViewMatchers.withId(R.id.username))
                .perform(typeText("Eleni"),closeSoftKeyboard());
        onView(withId(R.id.skipButton))
                .perform(click());
        Thread.sleep(4000);
    }

    @Test
    public void loginUser() throws InterruptedException {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.example.mymusicapp", appContext.getPackageName());

        // Type text and then press the button.
        onView(ViewMatchers.withId(R.id.username))
                .perform(typeText("Anonopgrc"));
        onView(ViewMatchers.withId(R.id.password))
                .perform(typeText("mission007"),closeSoftKeyboard());
        onView(withId(R.id.loginButton))
                .perform(click());
        Thread.sleep(4000);
    }



}