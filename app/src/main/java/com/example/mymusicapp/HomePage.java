package com.example.mymusicapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mymusicapp.artistmood.ArtistMoods;
import com.example.mymusicapp.fetchartistinfo.SearchAlbumsAndMusicVideosByArtistName;
import com.example.mymusicapp.fetchartistinfo.SearchArtistByName;
import com.example.mymusicapp.useraccount.Login;
import com.example.mymusicapp.useraccount.UserAccount;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class HomePage extends AppCompatActivity {

    CustomAdapter customAdapter;

    //for favorites
    ToggleButton tglBtn1, tglBtn2, tglBtn3, tglBtn4, tglBtn5, tglBtn6, tglBtn7, tglBtn8, tglBtn9;
    Button fvrtlistBtn,moodButton;
    TextView txtView1, txtView2, txtView3, txtView4, txtView5, txtView6, txtView7, txtView8, txtView9;
    String artNm1, artNm2, artNm3, artNm4, artNm5, artNm6, artNm7, artNm8, artNm9;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        moodButton = (Button) findViewById(R.id.moodButton);
        //for favorites
        sp = getSharedPreferences("MyUserPref", Context.MODE_PRIVATE);
        fvrtlistBtn = findViewById(R.id.btnFvrList);
        tglBtn1 = findViewById(R.id.btn1);
        tglBtn2 = findViewById(R.id.btn2);
        tglBtn3 = findViewById(R.id.btn3);
        tglBtn4 = findViewById(R.id.btn4);
        tglBtn5 = findViewById(R.id.btn5);
        tglBtn6 = findViewById(R.id.btn6);
        tglBtn7 = findViewById(R.id.btn7);
        tglBtn8 = findViewById(R.id.btn8);
        tglBtn9 = findViewById(R.id.btn9);
        txtView1 = findViewById(R.id.textView1);
        txtView2 = findViewById(R.id.textView2);
        txtView3 = findViewById(R.id.textView3);
        txtView4 = findViewById(R.id.textView4);
        txtView5 = findViewById(R.id.textView5);
        txtView6 = findViewById(R.id.textView6);
        txtView7 = findViewById(R.id.textView7);
        txtView8 = findViewById(R.id.textView8);
        txtView9 = findViewById(R.id.textView9);
        //for favorites
        fvrtlistBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomePage.this, FavoriteList.class);
                startActivity(intent);
            }
        });
        tglBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tglBtn1.isChecked()) {
                    artNm1 = txtView1.getText().toString();
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("artNm1", artNm1);
                    editor.commit();
                    Toast.makeText(HomePage.this, "Saved.", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.remove("artNm1");
                    editor.commit();
                    Toast.makeText(HomePage.this, "Deleted", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tglBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tglBtn2.isChecked()) {
                    artNm2 = txtView2.getText().toString();
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("artNm2", artNm2);
                    editor.commit();
                    Toast.makeText(HomePage.this, "Saved.", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.remove("artNm2");
                    editor.commit();
                    Toast.makeText(HomePage.this, "Deleted", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tglBtn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tglBtn3.isChecked()) {
                    artNm3 = txtView3.getText().toString();
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("artNm3", artNm3);
                    editor.commit();
                    Toast.makeText(HomePage.this, "Saved.", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.remove("artNm3");
                    editor.commit();
                    Toast.makeText(HomePage.this, "Deleted", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tglBtn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tglBtn4.isChecked()) {
                    artNm4 = txtView4.getText().toString();
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("artNm4", artNm4);
                    editor.commit();
                    Toast.makeText(HomePage.this, "Saved.", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.remove("artNm4");
                    editor.commit();
                    Toast.makeText(HomePage.this, "Deleted", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tglBtn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tglBtn5.isChecked()) {
                    artNm5 = txtView5.getText().toString();
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("artNm5", artNm5);
                    editor.commit();
                    Toast.makeText(HomePage.this, "Saved.", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.remove("artNm5");
                    editor.commit();
                    Toast.makeText(HomePage.this, "Deleted", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tglBtn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tglBtn6.isChecked()) {
                    artNm6 = txtView6.getText().toString();
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("artNm6", artNm6);
                    editor.commit();
                    Toast.makeText(HomePage.this, "Saved.", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.remove("artNm6");
                    editor.commit();
                    Toast.makeText(HomePage.this, "Deleted", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tglBtn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tglBtn7.isChecked()) {
                    artNm7 = txtView7.getText().toString();
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("artNm7", artNm7);
                    editor.commit();
                    Toast.makeText(HomePage.this, "Saved.", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.remove("artNm7");
                    editor.commit();
                    Toast.makeText(HomePage.this, "Deleted", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tglBtn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tglBtn8.isChecked()) {
                    artNm8 = txtView8.getText().toString();
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("artNm8", artNm8);
                    editor.commit();
                    Toast.makeText(HomePage.this, "Saved.", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.remove("artNm8");
                    editor.commit();
                    Toast.makeText(HomePage.this, "Deleted", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tglBtn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tglBtn9.isChecked()) {
                    artNm9 = txtView9.getText().toString();
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("artNm9", artNm9);
                    editor.commit();
                    Toast.makeText(HomePage.this, "Saved.", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences.Editor editor = sp.edit();
                    editor.remove("artNm9");
                    editor.commit();
                    Toast.makeText(HomePage.this, "Deleted", Toast.LENGTH_SHORT).show();
                }
            }
        });

            moodButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(HomePage.this, ArtistMoods.class));
                }
            });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem menuItem = menu.findItem(R.id.search_view);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                customAdapter.getFilter().filter(newText);
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search_view) {
            return true;
        }
        if (id == R.id.homePage) {
            Intent intent = new Intent(HomePage.this, HomePage.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.searchByName) {
            Intent intent = new Intent(HomePage.this, SearchArtistByName.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.searchAlbums) {
            Intent intent = new Intent(HomePage.this, SearchAlbumsAndMusicVideosByArtistName.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.trending_songs) {
            Intent intent = new Intent(HomePage.this, TrendingSongs.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.favorite_list) {
            Intent intent = new Intent(HomePage.this, FavoriteList.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.logout) {
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(HomePage.this, Login.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.user_Account) {
            Intent intent = new Intent(HomePage.this, UserAccount.class);
            startActivity(intent);
            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    //for favorites
    public void onDefaultToggleClick(View view) {
        Toast.makeText(this, "DefaultToggle", Toast.LENGTH_SHORT).show();
    }

    private class CustomAdapter extends BaseAdapter implements Filterable {

        private final List<ItemsModel> itemsModelList;
        private List<ItemsModel> itemsModelListFiltered;
        private final Context context;

        public CustomAdapter(List<ItemsModel> itemsModelList, Context context) {
            this.itemsModelList = itemsModelList;
            this.itemsModelListFiltered = itemsModelList;
            this.context = context;
        }

        @Override
        public int getCount() {
            return itemsModelListFiltered.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = getLayoutInflater().inflate(R.layout.row_data, null);

            TextView name = view1.findViewById(R.id.artists);
            ImageView image = view1.findViewById(R.id.imageView);

            name.setText(itemsModelListFiltered.get(i).getName());
            image.setImageResource(itemsModelListFiltered.get(i).getImage());

            view1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(HomePage.this, ListdataActivity.class).putExtra("item", itemsModelListFiltered.get(i)));
                }
            });

            return view1;
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();

                    if (constraint == null || constraint.length() == 0) {
                        filterResults.count = itemsModelList.size();
                        filterResults.values = itemsModelList;
                    } else {
                        String searchStr = constraint.toString().toLowerCase();
                        List<ItemsModel> resultData = new ArrayList<>();
                        for (ItemsModel itemsModel : itemsModelList) {
                            if (itemsModel.getName().contains(searchStr)) {
                                resultData.add(itemsModel);
                            }
                            filterResults.count = resultData.size();
                            filterResults.values = resultData;
                        }
                    }

                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {

                    itemsModelListFiltered = (List<ItemsModel>) results.values;
                    notifyDataSetChanged();

                }
            };
            return filter;
        }
    }
}