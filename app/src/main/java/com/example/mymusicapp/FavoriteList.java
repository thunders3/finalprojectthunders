package com.example.mymusicapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.mymusicapp.fetchartistinfo.SearchAlbumsAndMusicVideosByArtistName;
import com.example.mymusicapp.fetchartistinfo.SearchArtistByName;
import com.example.mymusicapp.useraccount.Login;
import com.example.mymusicapp.useraccount.UserAccount;
import com.google.firebase.auth.FirebaseAuth;

public class FavoriteList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_list);
        SharedPreferences sp = getApplicationContext().getSharedPreferences("MyUserPref", Context.MODE_PRIVATE);

        //1
        TextView t1;
        t1 = findViewById(R.id.textView1_1);
        String artNm1 = sp.getString("artNm1","");
        t1.setText(artNm1);

        //2
        TextView t2;
        t2 = findViewById(R.id.textView1_2);
        String artNm2 = sp.getString("artNm2","");
        t2.setText(artNm2);

        //3
        TextView t3;
        t3 = findViewById(R.id.textView1_3);
        String artNm3 = sp.getString("artNm3","");
        t3.setText(artNm3);

        //4
        TextView t4;
        t4 = findViewById(R.id.textView1_4);
        String artNm4 = sp.getString("artNm4","");
        t4.setText(artNm4);

        //5
        TextView t5;
        t5 = findViewById(R.id.textView1_5);
        String artNm5 = sp.getString("artNm5","");
        t5.setText(artNm5);

        //6
        TextView t6;
        t6 = findViewById(R.id.textView1_6);
        String artNm6 = sp.getString("artNm6","");
        t6.setText(artNm6);

        //7
        TextView t7;
        t7 = findViewById(R.id.textView1_7);
        String artNm7 = sp.getString("artNm7","");
        t7.setText(artNm7);

        //8
        TextView t8;
        t8 = findViewById(R.id.textView1_8);
        String artNm8 = sp.getString("artNm8","");
        t8.setText(artNm8);

        //9
        TextView t9;
        t9 = findViewById(R.id.textView1_9);
        String artNm9 = sp.getString("artNm9","");
        t9.setText(artNm9);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.search_view){
            return true;
        }
        if(id == R.id.homePage) {
            Intent intent = new Intent(FavoriteList.this, HomePage.class);
            startActivity(intent);
            finish();
            return true;
        }
        if(id == R.id.searchByName) {
            Intent intent = new Intent(FavoriteList.this, SearchArtistByName.class);
            startActivity(intent);
            finish();
            return true;
        }
        if(id == R.id.searchAlbums) {
            Intent intent = new Intent(FavoriteList.this, SearchAlbumsAndMusicVideosByArtistName.class);
            startActivity(intent);
            finish();
            return true;
        }
        if(id == R.id.trending_songs) {
            Intent intent = new Intent(FavoriteList.this, TrendingSongs.class);
            startActivity(intent);
            finish();
            return true;
        }
        if(id == R.id.favorite_list) {
            Intent intent = new Intent(FavoriteList.this, FavoriteList.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.logout) {
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(FavoriteList.this, Login.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.user_Account) {
            Intent intent = new Intent(FavoriteList.this, UserAccount.class);
            startActivity(intent);
            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

}