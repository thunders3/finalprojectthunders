package com.example.mymusicapp.artistmood;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.mymusicapp.FavoriteList;
import com.example.mymusicapp.HomePage;
import com.example.mymusicapp.R;
import com.example.mymusicapp.TrendingSongs;
import com.example.mymusicapp.fetchartistinfo.SearchAlbumsAndMusicVideosByArtistName;
import com.example.mymusicapp.fetchartistinfo.SearchArtistByName;

public class ArtistMoods extends AppCompatActivity {

    Button happyBtn, grittyBtn, angryBtn, sadBtn, partyBtn;


    ImageView happyMood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_moods);


        happyBtn = findViewById(R.id.happyBtn);
        happyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ArtistMoods.this, HappyActivity.class);
                startActivity(intent);
            }
        });

        grittyBtn = findViewById(R.id.grittyBtn);
        grittyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ArtistMoods.this, GrittyActivity.class);
                startActivity(intent);
            }
        });

        angryBtn = findViewById(R.id.angryBtn);
        angryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ArtistMoods.this, AngryActivity.class);
                startActivity(intent);
            }
        });

        sadBtn = findViewById(R.id.sadBtn);
        sadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ArtistMoods.this, SadActivity.class);
                startActivity(intent);
            }
        });

        partyBtn = findViewById(R.id.partyBtn);
        partyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ArtistMoods.this, PartyActivity.class);
                startActivity(intent);
            }
        });

    }
}