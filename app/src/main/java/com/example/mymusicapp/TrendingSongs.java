package com.example.mymusicapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.mymusicapp.fetchartistinfo.SearchAlbumsAndMusicVideosByArtistName;
import com.example.mymusicapp.fetchartistinfo.SearchArtistByName;
import com.example.mymusicapp.useraccount.Login;
import com.google.firebase.auth.FirebaseAuth;

public class TrendingSongs extends AppCompatActivity {
    private TextView mTextViewResult;
    private RequestQueue requestQueue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trending_songs);
        mTextViewResult = findViewById(R.id.text_view_result);
        requestQueue = Volley.newRequestQueue(this);
        parsedata();
    }
    private void parsedata()
    {

        String url = "https://api.npoint.io/473270cd1a1ab6064ce5";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("Charts");
                            for (int i =0;i<jsonArray.length();i++)
                            {
                                JSONObject Charts=jsonArray.getJSONObject(i);
                                String artist = Charts.getString("name");
                                String song = Charts.getString("Song");
                                String listeners = Charts.getString("listeners");
                                String playcount = Charts.getString("playcount");
                                mTextViewResult.append("\nName: "+ artist +"\n Song:"+ song + "\nListeners: " + listeners + "\nPlaycount: " +playcount +"\n");
                            }
                        } catch (JSONException e) {
                            mTextViewResult.setText("Failed");
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
                mTextViewResult.setText("Error");
            }
        });
        requestQueue.add(request);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.homePage) {
            Intent intent = new Intent(TrendingSongs.this, HomePage.class);
            startActivity(intent);
            finish();
            return true;
        }
        if(id == R.id.searchByName) {
            Intent intent = new Intent(TrendingSongs.this, SearchArtistByName.class);
            startActivity(intent);
            finish();
            return true;
        }
        if(id == R.id.searchAlbums) {
            Intent intent = new Intent(TrendingSongs.this, SearchAlbumsAndMusicVideosByArtistName.class);
            startActivity(intent);
            finish();
            return true;
        }
        if(id == R.id.trending_songs) {
            Intent intent = new Intent(TrendingSongs.this, TrendingSongs.class);
            startActivity(intent);
            finish();
            return true;
        }
        if(id == R.id.favorite_list) {
            Intent intent = new Intent(TrendingSongs.this, FavoriteList.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.logout) {
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(TrendingSongs.this, Login.class);
            startActivity(intent);
            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


}