package com.example.mymusicapp.fetchartistinfo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.mymusicapp.FavoriteList;
import com.example.mymusicapp.HomePage;
import com.example.mymusicapp.R;
import com.example.mymusicapp.TrendingSongs;
import com.example.mymusicapp.useraccount.Login;
import com.example.mymusicapp.useraccount.UserAccount;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class SearchArtistByName extends AppCompatActivity {

    Button btn_getArtistID, btn_getInfoByArtistID, btn_getInfoByArtistName;
    EditText et_dataInput;
    ListView lv_artistData;
    ImageView imageOfArtist;

    ArrayList<String> myArrayList;
    ArrayAdapter<String> myArrayAdapterList;
    ArrayAdapter<String> myArrayAdapterList2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_artist_by_name);

        btn_getArtistID = findViewById(R.id.btn_getArtistID);
        btn_getInfoByArtistID = findViewById(R.id.btn_getInfoByArtistID);
        btn_getInfoByArtistName = findViewById(R.id.btn_getInfoByArtistName);
        et_dataInput = findViewById(R.id.et_dataInput);
        lv_artistData = findViewById(R.id.lv_artistData);
        imageOfArtist = findViewById(R.id.imageOfArtist);
        final ArtistDataService artistDataService = new ArtistDataService(SearchArtistByName.this);

        //CLICK LISTENERS FOR EACH BUTTON

        btn_getArtistID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               artistDataService.getArtistID(et_dataInput.getText().toString(), new ArtistDataService.VolleyResponseListener() {
                   @Override
                   public void onError(String message) {
                       Toast.makeText(SearchArtistByName.this, "Something wrong" , Toast.LENGTH_SHORT).show();

                   }

                   @Override
                   public void onResponse(String artistID) {
                       Toast.makeText(SearchArtistByName.this, "Return and ID of " + artistID, Toast.LENGTH_SHORT).show();

                   }
               });
            }
        });

        btn_getInfoByArtistID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                artistDataService.getArtistInfoByID(et_dataInput.getText().toString(),imageOfArtist, new ArtistDataService.ArtistByIDResponse() {
                    @Override
                    public void onError(String message) {
                        Toast.makeText(SearchArtistByName.this, "Somethig Wrong", Toast.LENGTH_SHORT).show();


                    }

                    @Override
                    public void onResponse(List<ArtistInformationModel> artistInformationModels) {
                        //put the entire list into the listview control

                        ArrayAdapter arrayAdapter = new ArrayAdapter(SearchArtistByName.this, android.R.layout.simple_list_item_1, artistInformationModels);
                        lv_artistData.setAdapter(arrayAdapter);

                    }
                });

            }
        });

        btn_getInfoByArtistName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                artistDataService.getArtistInfoByName(et_dataInput.getText().toString(),imageOfArtist, new ArtistDataService.GetArtistByNameCallback() {
                    @Override
                    public void onError(String message) {
                        Toast.makeText(SearchArtistByName.this, "Somethig Wrong", Toast.LENGTH_SHORT).show();


                    }

                    @Override
                    public void onResponse(List<ArtistInformationModel> artistInformationModels) {
                        //put the entire list into the listview control

                        ArrayAdapter arrayAdapter = new ArrayAdapter(SearchArtistByName.this, android.R.layout.simple_list_item_1, artistInformationModels);
                        lv_artistData.setAdapter(arrayAdapter);

                    }
                });            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search_view) {
            return true;
        }
        if (id == R.id.homePage) {
            Intent intent = new Intent(SearchArtistByName.this, HomePage.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.searchByName) {
            Intent intent = new Intent(SearchArtistByName.this, SearchArtistByName.class);
            startActivity(intent);
            finish();
            return true;
        }
        if(id == R.id.searchAlbums) {
            Intent intent = new Intent(SearchArtistByName.this, SearchAlbumsAndMusicVideosByArtistName.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.trending_songs) {
            Intent intent = new Intent(SearchArtistByName.this, TrendingSongs.class);
            startActivity(intent);
            finish();
            return true;
        }
        if(id == R.id.favorite_list) {
            Intent intent = new Intent(SearchArtistByName.this, FavoriteList.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.logout) {
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(SearchArtistByName.this, Login.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.user_Account) {
            Intent intent = new Intent(SearchArtistByName.this, UserAccount.class);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}