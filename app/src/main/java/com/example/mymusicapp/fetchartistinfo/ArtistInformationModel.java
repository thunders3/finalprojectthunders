package com.example.mymusicapp.fetchartistinfo;

public class ArtistInformationModel {
    private String idArtist;
    private String strArtist;
    private String intBornYear;
    private String strStyle;
    private String strBiographyEN;
    private String strArtistThumb;
    private String strMood;

    public ArtistInformationModel(String idArtist, String strArtist, String intBornYear, String strStyle, String strBiographyEN, String strArtistThumb, String strMood) {
        this.idArtist = idArtist;
        this.strArtist = strArtist;
        this.intBornYear = intBornYear;
        this.strStyle = strStyle;
        this.strBiographyEN = strBiographyEN;
        this.strArtistThumb = strArtistThumb;
        this.strMood = strMood;
    }

    public ArtistInformationModel() {
    }

    @Override
    public String toString() {
        return  "Artist ID:" + idArtist + " ,Name:" + strArtist +
                " ,BornYear:" + intBornYear +
                " ,Music Style:" + strStyle +
                " ,Biography:" + strBiographyEN ;
    }

    public String getIdArtist() {
        return idArtist;
    }

    public void setIdArtist(String idArtist) {
        this.idArtist = idArtist;
    }

    public String getStrArtist() {
        return strArtist;
    }

    public void setStrArtist(String strArtist) {
        this.strArtist = strArtist;
    }

    public String getIntBornYear() {
        return intBornYear;
    }

    public void setIntBornYear(String intBornYear) {
        this.intBornYear = intBornYear;
    }

    public String getStrStyle() {
        return strStyle;
    }

    public void setStrStyle(String strStyle) {
        this.strStyle = strStyle;
    }

    public String getStrBiographyEN() {
        return strBiographyEN;
    }

    public void setStrBiographyEN(String strBiographyEN) {
        this.strBiographyEN = strBiographyEN;
    }

    public String getStrArtistThumb() {
        return strArtistThumb;
    }

    public void setStrArtistThumb(String strArtistThumb) {
        this.strArtistThumb = strArtistThumb;
    }

    public String getStrMood() {
        return strMood;
    }

    public void setStrMood(String strMood) {
        this.strMood = strMood;
    }
}
