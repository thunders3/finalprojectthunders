package com.example.mymusicapp.fetchartistinfo;

public class ArtistMusicVideosModel {
    private String strTrack;
    private String strMusicVid;

    public ArtistMusicVideosModel(String strTrack, String strMusicVid) {
        this.strTrack = strTrack;
        this.strMusicVid = strMusicVid;
    }

    public ArtistMusicVideosModel() {
    }

    @Override
    public String toString() {
        return "Track Name:  " + strTrack + "  " +
                ",Music Video: " + strMusicVid ;
    }

    public String getStrTrack() {
        return strTrack;
    }

    public void setStrTrack(String strTrack) {
        this.strTrack = strTrack;
    }

    public String getStrMusicVid() {
        return strMusicVid;
    }

    public void setStrMusicVid(String strMusicVid) {
        this.strMusicVid = strMusicVid;
    }
}
