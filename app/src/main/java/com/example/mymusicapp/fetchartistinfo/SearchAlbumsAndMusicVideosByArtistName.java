package com.example.mymusicapp.fetchartistinfo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mymusicapp.FavoriteList;
import com.example.mymusicapp.HomePage;
import com.example.mymusicapp.R;
import com.example.mymusicapp.TrendingSongs;
import com.example.mymusicapp.useraccount.Login;
import com.example.mymusicapp.useraccount.UserAccount;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class SearchAlbumsAndMusicVideosByArtistName extends AppCompatActivity {

    Button artistID,albumsByID,musicVideos;
    EditText et_dataInPut;
    ListView lv_ArtistData;
    TextView musicVideoURL;
    ArrayList<String> myArrayList = new ArrayList<>();
    ArrayAdapter<String> myArrayAdapterList;
    ArrayAdapter<String> myArrayAdapterList2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_albums_and_music_vid_by_artist_id);

        artistID = findViewById(R.id.artistID);
        albumsByID = findViewById(R.id.albumsByID);
        musicVideos = findViewById(R.id.musicVideos);
        et_dataInPut = findViewById(R.id.et_dataInPut);
        lv_ArtistData = findViewById(R.id.lv_ArtistData);
        musicVideoURL = findViewById(R.id.musicVideoURL);
        final ArtistDataService artistDataService = new ArtistDataService(SearchAlbumsAndMusicVideosByArtistName.this);


        //CLICK LISTENERS FOR EACH BUTTON

        //GETTING ARTIST ID
        artistID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                artistDataService.getArtistID(et_dataInPut.getText().toString(), new ArtistDataService.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        Toast.makeText(SearchAlbumsAndMusicVideosByArtistName.this, "Somethingbb wrong", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onResponse(String artistID) {
                        Toast.makeText(SearchAlbumsAndMusicVideosByArtistName.this, "Return and ID of " + artistID, Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });


        //GETTING ALBUMS BY ARTIST NAME
        albumsByID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                artistDataService.getAlbumByName(et_dataInPut.getText().toString(), new ArtistDataService.GetAlbumByNameCallback() {
                    @Override
                    public void onError(String message) {
                        Toast.makeText(SearchAlbumsAndMusicVideosByArtistName.this, "Somethig Wrong", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onResponse(List<ArtistAlbumsModel> artistAlbumsModels) {
                        //put the entire list into the listview control
                        ArrayAdapter arrayAdapter = new ArrayAdapter(SearchAlbumsAndMusicVideosByArtistName.this, android.R.layout.simple_list_item_1, artistAlbumsModels);
                        lv_ArtistData.setAdapter(arrayAdapter);

                    }
                });
            }
        });


        //GETTING MUSIC VIDEOS BY ARTIST NAME
        musicVideos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                artistDataService.getMusicVideosByName(et_dataInPut.getText().toString(), new ArtistDataService.GetMusicVideosByNameCallback() {
                    @Override
                    public void onError(String message) {
                        Toast.makeText(SearchAlbumsAndMusicVideosByArtistName.this, "Somethig Wrong", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(List<ArtistMusicVideosModel> artistMusicVideosModels) {

                        ArrayAdapter arrayAdapter = new ArrayAdapter(SearchAlbumsAndMusicVideosByArtistName.this, android.R.layout.simple_list_item_1, artistMusicVideosModels);
                        lv_ArtistData.setAdapter(arrayAdapter);
                        lv_ArtistData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(artistMusicVideosModels.get(position).getStrMusicVid()));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search_view) {
            return true;
        }
        if (id == R.id.homePage) {
            Intent intent = new Intent(SearchAlbumsAndMusicVideosByArtistName.this, HomePage.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.searchByName) {
            Intent intent = new Intent(SearchAlbumsAndMusicVideosByArtistName.this, SearchArtistByName.class);
            startActivity(intent);
            finish();
            return true;
        }
        if(id == R.id.searchAlbums) {
            Intent intent = new Intent(SearchAlbumsAndMusicVideosByArtistName.this, SearchAlbumsAndMusicVideosByArtistName.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.trending_songs) {
            Intent intent = new Intent(SearchAlbumsAndMusicVideosByArtistName.this, TrendingSongs.class);
            startActivity(intent);
            finish();
            return true;
        }
        if(id == R.id.favorite_list) {
            Intent intent = new Intent(SearchAlbumsAndMusicVideosByArtistName.this, FavoriteList.class);
            startActivity(intent);
            finish();
            return true;
        }

        if (id == R.id.logout) {
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(SearchAlbumsAndMusicVideosByArtistName.this, Login.class);
            startActivity(intent);
            finish();
            return true;
        }
        if (id == R.id.user_Account) {
            Intent intent = new Intent(SearchAlbumsAndMusicVideosByArtistName.this, UserAccount.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}