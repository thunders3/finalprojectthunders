package com.example.mymusicapp.fetchartistinfo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mymusicapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FetchArtistData extends AppCompatActivity {
    public static TextView textView;
    private Button button;
    private RequestQueue requestQueue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fetch_artist_data);

        textView = (TextView) findViewById(R.id.fetchedData);
        button = (Button) findViewById(R.id.click);
        requestQueue = Volley.newRequestQueue(this);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parsedata();
            }
        });


    }

    private void parsedata() {
        String url = "https://api.npoint.io/a53386d197aaa3eafe52";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray=response.getJSONArray("Artist");
                            for (int i =0;i<jsonArray.length();i++){
                                JSONObject Artist=jsonArray.getJSONObject(i);
                                String name = Artist.getString("name");
                                String Song = Artist.getString("Song");
                                String Nationality = Artist.getString("Nationality");
                                textView.append(name + "," + Song + "," + Nationality );
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error){

                error.printStackTrace();
                textView.setText("Error");
            }
        });
        requestQueue.add(request);
    }
}
