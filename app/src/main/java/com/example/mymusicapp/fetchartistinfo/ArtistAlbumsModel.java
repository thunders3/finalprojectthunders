package com.example.mymusicapp.fetchartistinfo;

public class ArtistAlbumsModel {
    private String idArtist;
    private  String strAlbum;
    private String intYearReleased;
    private String strStyle;
    private String strMood;

    public ArtistAlbumsModel(String idArtist, String strAlbum, String intYearReleased, String strStyle, String strMood) {
        this.idArtist = idArtist;
        this.strAlbum = strAlbum;
        this.intYearReleased = intYearReleased;
        this.strStyle = strStyle;
        this.strMood = strMood;
    }

    public ArtistAlbumsModel() {
    }

    @Override
    public String toString() {
        return "Artist ID: " + idArtist +
                " ,Album: " + strAlbum +
                " ,Year Released:" + intYearReleased +
                " ,Style: " + strStyle +
                " ,Mood:" + strMood ;
    }

    public String getIdArtist() { return idArtist; }

    public void setIdArtist(String idArtist) { this.idArtist = idArtist; }

    public String getStrAlbum() {
        return strAlbum;
    }

    public void setStrAlbum(String strAlbum) {
        this.strAlbum = strAlbum;
    }

    public String getIntYearReleased() {
        return intYearReleased;
    }

    public void setIntYearReleased(String intYearReleased) {
        this.intYearReleased = intYearReleased;
    }

    public String getStrStyle() {
        return strStyle;
    }

    public void setStrStyle(String strStyle) {
        this.strStyle = strStyle;
    }

    public String getStrMood() {
        return strMood;
    }

    public void setStrMood(String strMood) {
        this.strMood = strMood;
    }
}
