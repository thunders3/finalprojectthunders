package com.example.mymusicapp.fetchartistinfo;

import android.content.Context;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ArtistDataService {

    public static final String QUERY_FOR_ARTIST_ID = "https://www.theaudiodb.com/api/v1/json/2/search.php?s=";
    public static final String QUERY_FOR_ARTIST_BY_ID = "https://theaudiodb.com/api/v1/json/2/artist.php?i=";
    public static final String QUERY_FOR_ALBUM_BY_ARTIST_ID = "https://theaudiodb.com/api/v1/json/2/album.php?i=";
    public static final String QUERY_FOR_MUSIC_VIDEOS = "https://theaudiodb.com/api/v1/json/2/mvid.php?i=";
    Context context;
    String artistID;
    List<ArtistInformationModel> artistInformationModels;

    public ArtistDataService(Context context) {
        this.context = context;
    }

    //GET ARTIST ID
    public void getArtistID(String artistName, VolleyResponseListener volleyResponseListener) {
        String url = QUERY_FOR_ARTIST_ID + artistName;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                artistID = "";
                try {
                    JSONArray jsonArray = response.getJSONArray("artists");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject artistInfo = jsonArray.getJSONObject(i);
                        artistID = artistInfo.getString("idArtist");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Toast.makeText(context, "Artist ID =" + artistID, Toast.LENGTH_SHORT).show();
                volleyResponseListener.onResponse(artistID);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context, "Something Wrong.", Toast.LENGTH_SHORT).show();
                volleyResponseListener.onError("Something wrong");
            }
        });
        MySingleton.getInstance(context).addToRequestQueue(request);

    }


    //GET INFO BY ID
    public void getArtistInfoByID(String artistID, ImageView imageView, ArtistByIDResponse artistByIDResponse) {

        List<ArtistInformationModel> artistInformationModels = new ArrayList<>();
        String url = QUERY_FOR_ARTIST_BY_ID + artistID;

        //get the json object
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // Toast.makeText(context, response.toString(), Toast.LENGTH_SHORT).show();
                try {
                    JSONArray jsonArray = response.getJSONArray("artists");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ArtistInformationModel one_artist = new ArtistInformationModel();

                        JSONObject artistInfo = jsonArray.getJSONObject(i);
                        one_artist.setIdArtist(artistInfo.getString("idArtist"));
                        one_artist.setStrArtist(artistInfo.getString("strArtist"));
                        one_artist.setIntBornYear(artistInfo.getString("intBornYear"));
                        one_artist.setStrStyle(artistInfo.getString("strStyle"));
                        one_artist.setStrBiographyEN(artistInfo.getString("strBiographyEN"));
                        //THIS IS ANOTHER WAY
//                       artistID= artistInfo.getString("strArtist")+
//                        artistInfo.getString("intBornYear");
                        artistInformationModels.add(one_artist);
                        Picasso.get().load(artistInfo.getString("strArtistThumb")).into(imageView);

                    }
                    artistByIDResponse.onResponse(artistInformationModels);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MySingleton.getInstance(context).addToRequestQueue(request);
        //get the property called "artists" which is an array
        //get item in the array and assign it to a new ArtistInformation object
    }

    //GET ALBUMS BY ID
    public void getAlbumsByArtistID(String artistID, AlbumsByIDResponse albumsByIDResponse) {

        List<ArtistAlbumsModel> artistAlbumsModels = new ArrayList<>();
        String url = QUERY_FOR_ALBUM_BY_ARTIST_ID + artistID;

        //get the json object

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("album");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ArtistAlbumsModel one_album = new ArtistAlbumsModel();

                        JSONObject artistAlbums = jsonArray.getJSONObject(i);
                        one_album.setIdArtist(artistAlbums.getString("idArtist"));
                        one_album.setStrAlbum(artistAlbums.getString("strAlbum"));
                        one_album.setIntYearReleased(artistAlbums.getString("intYearReleased"));
                        one_album.setStrStyle(artistAlbums.getString("strStyle"));
                        one_album.setStrMood(artistAlbums.getString("strMood"));

                        artistAlbumsModels.add(one_album);

                    }
                    albumsByIDResponse.onResponse(artistAlbumsModels);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MySingleton.getInstance(context).addToRequestQueue(request);
    }

    //GET VIDEOS BY ID
    public void getMusicVideosByArtistID(String artistID, MusicVideosByIDResponse musicVideosByIDResponse) {

        List<ArtistMusicVideosModel> artistMusicVideosModels = new ArrayList<>();
        String url = QUERY_FOR_MUSIC_VIDEOS + artistID;

        //get the json object
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //String urlYoutube = "";
                try {
                    JSONArray jsonArray = response.getJSONArray("mvids");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        ArtistMusicVideosModel one_track = new ArtistMusicVideosModel();

                        JSONObject artistMusicVideos = jsonArray.getJSONObject(i);
                        one_track.setStrTrack(artistMusicVideos.getString("strTrack"));
                        one_track.setStrMusicVid(artistMusicVideos.getString("strMusicVid"));
                        artistMusicVideosModels.add(one_track);
                        //urlYoutube = artistMusicVideos.getString("strMusicVid");

                    }
                    musicVideosByIDResponse.onResponse(artistMusicVideosModels);
                   // musicVideoByIDResponse.onResponse(artistMusicVideosModels.setM);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MySingleton.getInstance(context).addToRequestQueue(request);

    }

    //GET VIDEOS BY NAME
    public void getMusicVideosByName(String artistName, GetMusicVideosByNameCallback getMusicVideosByNameCallback) {
        //fetch the artist id given the artist name
        getArtistID(artistName, new VolleyResponseListener() {
            @Override
            public void onError(String message) {

            }

            @Override
            public void onResponse(String artistID) {
                getMusicVideosByArtistID(artistID, new MusicVideosByIDResponse() {
                    @Override
                    public void onError(String message) {

                    }

                    @Override
                    public void onResponse(List<ArtistMusicVideosModel> artistMusicVideosModels) {
                        getMusicVideosByNameCallback.onResponse(artistMusicVideosModels);

                    }
                });
            }
        });
    }

    //GET ALBUM BY NAME
    public void getAlbumByName(String artistName, GetAlbumByNameCallback getAlbumByNameCallback) {
        //fetch the artist id given the artist name
        getArtistID(artistName, new VolleyResponseListener() {
            @Override
            public void onError(String message) {

            }

            @Override
            public void onResponse(String artistID) {
                getAlbumsByArtistID(artistID, new AlbumsByIDResponse() {
                    @Override
                    public void onError(String message) {

                    }

                    @Override
                    public void onResponse(List<ArtistAlbumsModel> artistAlbumsModels) {
                        getAlbumByNameCallback.onResponse(artistAlbumsModels);

                    }
                });
            }
        });
    }

    //GET ARTIST INFO BY NAME
    public void getArtistInfoByName(String artistName, ImageView imageView, GetArtistByNameCallback getArtistByNameCallback) {
        //fetch the artist id given the artist name
        getArtistID(artistName, new VolleyResponseListener() {
            @Override
            public void onError(String message) {

            }

            @Override
            public void onResponse(String artistID) {
                getArtistInfoByID(artistID, imageView, new ArtistByIDResponse() {
                    @Override
                    public void onError(String message) {

                    }

                    @Override
                    public void onResponse(List<ArtistInformationModel> artistInformationModels) {
                        getArtistByNameCallback.onResponse(artistInformationModels);
                    }
                });
            }
        });

    }



    //INTERFACES
    public interface VolleyResponseListener {
        void onError(String message);

        void onResponse(String artistID);
    }

    public interface ArtistByIDResponse {
        void onError(String message);

        void onResponse(List<ArtistInformationModel> artistInformationModels);
    }


    //GET ALBUMS BY ARTIST ID
    public interface AlbumsByIDResponse {
        void onError(String message);

        void onResponse(List<ArtistAlbumsModel> artistAlbumsModels);
    }

    //GET MUSIC VIDEOS BY ARTIST ID
    public interface MusicVideosByIDResponse {
        void onError(String message);

        void onResponse(List<ArtistMusicVideosModel> artistMusicVideosModels);
    }


    public interface MusicVideoByIDResponse {
        void onError(String message);

        void onResponse(ArtistMusicVideosModel artistMusicVideosModel);
    }

    //GET MUSIC VIDEOS BY ARTIST NAME CALLBACK
    public interface GetMusicVideosByNameCallback {
        void onError(String message);

        void onResponse(List<ArtistMusicVideosModel> artistMusicVideosModels);
    }


    //GET ARTIST ALBUMS BY ARTIST NAME CALLBACK
    public interface GetAlbumByNameCallback {
        void onError(String message);

        void onResponse(List<ArtistAlbumsModel> artistAlbumsModels);
    }

    //GET ARTIST INFO BY ARTIST NAME
    public interface GetArtistByNameCallback {
        void onError(String message);

        void onResponse(List<ArtistInformationModel> artistInformationModels);
    }


}
