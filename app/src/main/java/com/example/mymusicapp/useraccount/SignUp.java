package com.example.mymusicapp.useraccount;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mymusicapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

public class SignUp extends AppCompatActivity implements View.OnClickListener{
    private TextView alreadyHaveAnAccount;
    EditText username,email,password;
    private FirebaseAuth mAuth;
    MaterialButton signUpButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mAuth = FirebaseAuth.getInstance();
        alreadyHaveAnAccount=(TextView) findViewById(R.id.alreadyHaveAnAccount);
        email = (EditText) findViewById(R.id.email);
        username = (EditText) findViewById(R.id.usernameSignUp);
        password = (EditText) findViewById(R.id.passwordSignUp);
        signUpButton = (MaterialButton) findViewById(R.id.signUpButton);
        alreadyHaveAnAccount.setOnClickListener(this);
        signUpButton.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.alreadyHaveAnAccount:
                startActivity(new Intent(this, Login.class));
                break;
            case R.id.signUpButton:
                registerUser();
                break;

        }

    }
    private void registerUser(){
        String Email = email.getText().toString().trim();
        String Username = username.getText().toString().trim();
        String Password = password.getText().toString().trim();
        if(Username.isEmpty()){
            username.setError("Username is required!");
            username.requestFocus();
            return;
        }
        if(Email.isEmpty()){
            email.setError("Email is required!");
            email.requestFocus();
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(Email).matches()){
            email.setError("Please provide a valid email!");
            username.requestFocus();
            return;

        }
        if(Password.length()<6){
            password.setError("Min password length must be 6 characters!");
            password.requestFocus();
            return;
        }


        mAuth.createUserWithEmailAndPassword(Email,Password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if(task.isSuccessful()){
                            User user = new User(Username,Email);

                            FirebaseDatabase.getInstance().getReference("Users")
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                    .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        Toast.makeText(SignUp.this, "User has been registered successfully!", Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(SignUp.this, "Failed to register! Try again!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }else{
                            Toast.makeText(SignUp.this, "Failed to register! Try again!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }
}