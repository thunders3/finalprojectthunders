package com.example.mymusicapp.useraccount;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mymusicapp.HomePage;
import com.example.mymusicapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class Login extends AppCompatActivity implements View.OnClickListener{
    private Button skipButton;
    private MaterialButton loginButton;
    private TextView forgotPass,register,email,password;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        email = findViewById(R.id.username);
        password = findViewById(R.id.password);
        skipButton = (Button) findViewById(R.id.skipButton);
        skipButton.setOnClickListener(this);
        loginButton = (MaterialButton) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(this);
        register = (TextView) findViewById(R.id.register);
        register.setOnClickListener(this);
        forgotPass = (TextView) findViewById(R.id.forgotpass);


        //Alternative Skins

        Button button1, button2, button3, button4, button5;
        final RelativeLayout relativeLayout;
        button1 = findViewById(R.id.btVar1);
        button2 = findViewById(R.id.btVar2);
        button3 = findViewById(R.id.btVar3);
        button4 = findViewById(R.id.btVar4);
        button5 = findViewById(R.id.btVar5);
        relativeLayout = findViewById(R.id.rlVar1);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // set the color to relative layout
                relativeLayout.setBackgroundResource(R.color.white);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // set the color to relative layout
                relativeLayout.setBackgroundResource(R.color.black);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // set the color to relative layout
                relativeLayout.setBackgroundResource(R.color.bg1);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // set the color to relative layout
                relativeLayout.setBackgroundResource(R.color.bg2);
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                relativeLayout.setBackgroundResource(R.color.bg3);
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.register:
                startActivity(new Intent(this,SignUp.class));
                break;
            case R.id.loginButton:
                userLogin();
                break;
            case R.id.skipButton:
                Toast.makeText(Login.this, "You may want to Login/Register!", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, HomePage.class));
                break;

        }

    }

    private void userLogin() {
        String Email = email.getText().toString().trim();
        String Password = password.getText().toString().trim();

        if(Email.isEmpty()){
            email.setError("Username is required!");
            email.requestFocus();
            return;
        }
        if(Password.isEmpty()){
            password.setError("Password is required!");
            password.requestFocus();
            return;
        }

        if(Password.length()<6){
            password.setError("Min password length must be 6 characters!");
            password.requestFocus();
            return;
        }

        mAuth.signInWithEmailAndPassword(Email,Password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    startActivity(new Intent(Login.this,HomePage.class));
                }else{
                    Toast.makeText(Login.this, "Failed to login!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}