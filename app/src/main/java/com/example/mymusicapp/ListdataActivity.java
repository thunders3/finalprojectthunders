package com.example.mymusicapp;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ListdataActivity extends AppCompatActivity {
    TextView name;
    ImageView image;
    ListView listView;
    ItemsModel itemsModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listdata);

        name = findViewById(R.id.listdata);
        image = findViewById(R.id.ImageView);

        Intent intent = getIntent();

        if(intent.getExtras() != null ){
            itemsModel = (ItemsModel) intent.getSerializableExtra("item");
            name.setText(itemsModel.getName());
            image.setImageResource(itemsModel.getImage());
        }

    }
}