package listofartists.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.mymusicapp.R;

import java.util.ArrayList;

import listofartists.ArtistAdapter;
import listofartists.ArtistItem;

public class HomeFragment extends Fragment {

    private ArrayList<ArtistItem> artistItems = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new ArtistAdapter(artistItems, getActivity()));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        artistItems.add(new ArtistItem(R.drawable.josephine, "Josephine", "0", "0"));
        artistItems.add(new ArtistItem(R.drawable.argyros, "Argyros", "1", "0"));
        artistItems.add(new ArtistItem(R.drawable.eminem, "Eminem", "2", "0"));
        artistItems.add(new ArtistItem(R.drawable.mitropanos, "Mitropanos", "3", "0"));


        return root;
    }
}


