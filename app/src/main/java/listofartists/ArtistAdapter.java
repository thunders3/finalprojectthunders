package listofartists;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mymusicapp.R;

import java.util.ArrayList;


public class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.ViewHolder> {

    private ArrayList<ArtistItem> artistItems;
    private Context context;
    private FavDB favDB;

    public ArtistAdapter(ArrayList<ArtistItem> artistItems, Context context) {
        this.artistItems = artistItems;
        this.context = context;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        favDB = new FavDB(context);
        //create table on first
        SharedPreferences prefs = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        boolean firstStart = prefs.getBoolean("firstStart", true);
        if (firstStart) {
            createTabelOnFirstStart();
        }

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item,
                parent, false);
        return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ArtistItem artistItem = artistItems.get(position);

        readCursorData(artistItem, holder);
        holder.imageView.setImageResource(artistItem.getImageResourse());
        holder.titleTextView.setText(artistItem.getTitle());
    }



    @Override
    public int getItemCount() {
        return artistItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView titleTextView;
        Button favBtn;



        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageView);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            favBtn = itemView.findViewById(R.id.favBtn);

            //add to fav btn

            favBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    ArtistItem artistItem = artistItems.get(position);

                    if (artistItem.getFavStatus().equals("0")) {
                        artistItem.setFavStatus("1");
                        favDB.insertIntoTheDatabase(artistItem.getTitle(), artistItem.getImageResourse(),
                                artistItem.getKey_id(), artistItem.getFavStatus());
                        favBtn.setBackgroundResource(R.drawable.ic_favorite_red_24);
                    } else {
                        artistItem.setFavStatus("0");
                        favDB.remove_fav(artistItem.getKey_id());
                        favBtn.setBackgroundResource(R.drawable.ic_favorite_shadow_24);
                    }
                }
            });
        }
    }

    private void createTabelOnFirstStart() {
        favDB.insertEmpty();

        SharedPreferences prefs = context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("firstStart", false);
        editor.apply();
    }
    private void readCursorData(ArtistItem artistItem, ViewHolder viewHolder) {
        Cursor cursor = favDB.read_all_data(artistItem.getKey_id());
        SQLiteDatabase db = favDB.getReadableDatabase();
        try {
            while (cursor.moveToNext()) {
                String item_fav_status = cursor.getString(cursor.getColumnIndex(FavDB.FAVORITE_STATUS));
                artistItem.setFavStatus(item_fav_status);

                //check fav status
                if (item_fav_status != null && item_fav_status.equals("1")) {
                    viewHolder.favBtn.setBackgroundResource(R.drawable.ic_favorite_red_24);
                }else if (item_fav_status != null && item_fav_status.equals("0")) {
                    viewHolder.favBtn.setBackgroundResource(R.drawable.ic_favorite_shadow_24);
                }
            }
        }finally {
            if (cursor != null && cursor.isClosed())
                cursor.close();
            db.close();

        }
    }

}
